package player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import javazoom.spi.mpeg.sampled.convert.DecodedMpegAudioInputStream;

/**
 * This class implements the main player. It can handle
 * any {@link Playable} object, and supports the PLAY,
 * STOP and FAST FORWARD operations.
 * 
 * @author Maria
 *
 */
public class Player implements Runnable {

	protected enum State {
		PLAY, PAUSE, STOP
	};
	
	public enum Event {
		INIT_PLAY, PAUSED, STOPPED, RESUMED
	};
	
	private Thread t;
	private Playable playable;
	private DecodedMpegAudioInputStream audioStream;
	private AudioFormat audioFormat;
	private List<PlayerListener> listeners;
	private State state;
	private boolean move_fwd;

	/**
	 * Class constructor.
	 */
	public Player() {
		move_fwd = false;
		state = State.STOP;
		audioStream = null;
		listeners = new ArrayList<PlayerListener>();
	}

	/**
	 * Reproduce the given song in a new thread.
	 */
	public void run() {

		SourceDataLine line = null;
		move_fwd = false;
		SourceDataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
		try {
			line = (SourceDataLine) AudioSystem.getLine(info);
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
		if (line != null) {
			try {
				line.open(audioFormat);
				line.start();
				int nBytesRead = 0;
				byte[] abData = new byte[1024];
				while((nBytesRead != -1)&&(!isStopped())) {
					synchronized(this) {
						while(isPaused())
							this.wait();
					}	
					if(move_fwd) {
						audioStream.skipFrames(framesToSkip());
						move_fwd = false;
					}
					nBytesRead = audioStream.read(abData, 0, abData.length);
					if (nBytesRead >= 0) {
						line.write(abData, 0, nBytesRead);
					}
				}
			} catch (LineUnavailableException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				line.drain();
				line.close();
			} finally {
				line.drain();
				line.close();
			}
			if(!isStopped())
				next();
		}
	}
	
	private long framesToSkip() {
		int sec = 10; //skip 10 seconds when fast forward
		long fps = Math.round((float)playable.getProperty("mp3.framerate.fps"));
		return (sec*fps);
	}

	private void init_play(Playable p) {
		if(p == null)
			return;
		if(!isStopped())
			stop();

		playable = p;
		audioStream = p.getInputStream();
		if(audioStream == null)
			return;
		audioFormat = audioStream.getFormat();

		t = new Thread(this, "Mp3 Player");
		state = State.PLAY;
		t.start();
		for(PlayerListener l : listeners)
			l.playerStatusChanged(Event.INIT_PLAY);
	}

	/**
	 * Play a given song. If the player was previously
	 * paused, then resume the song.
	 * 
	 * @param p any playable object
	 */
	public void play(Playable p) {
		if(isPaused()) {
			resume();
		} else if (isStopped()) {
			init_play(p);
		}
	}

	/**
	 * Pause the song.
	 */
	public void pause() {
		if(isPlay()) {
			state = State.PAUSE;
			for(PlayerListener l : listeners)
				l.playerStatusChanged(Event.PAUSED);
		}
		
	}

	private void resume() {
		state = State.PLAY;
		synchronized(this) {
			this.notify();
		}
		for(PlayerListener l : listeners)
			l.playerStatusChanged(Event.RESUMED);
	}

	/**
	 * Stop the song.
	 */
	public void stop() {
		if(isStopped())
			return;
		
		state = State.STOP;
		try {
			if(t.isAlive()) {
				synchronized(this) {
					this.notify();
				}
				t.join();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		audioStream = null;
		for(PlayerListener l : listeners)
			l.playerStatusChanged(Event.STOPPED);
	}
	
	/**
	 * Start the next song.
	 */
	public void next() {
		if(state == State.STOP)
			return;
		Playable next_song = playable.getNext();
		audioStream = null;
		state = State.STOP;
		if(next_song == null) {
			for(PlayerListener l : listeners)
				l.playerStatusChanged(Event.STOPPED);
		}
		else {
			init_play(next_song);
		}
	}

	/**
	 * Get the time elapsed in pretty printed format.
	 * 
	 * @return time elapsed
	 */
	public String getTimeElapsed() {
		String time;
		if(audioStream == null) {
			time = "--.--";
		} else {
			long all_sec = getmsElapsed()/1000000;
			long min = all_sec/60;
			long sec = all_sec%60;
			time = String.format("%02d:%02d", min, sec);
		}
		return time;
	}

	private long getmsElapsed() {
		if(audioStream == null)
			return 0;
		long ms = (long) audioStream.properties().get("mp3.position.microseconds");
		return ms;
	}

	/**
	 * Get the player position in percentage.
	 * 
	 * @return position in percentage
	 */
	public int getPercentage() {
		if(playable == null || audioStream == null)
			return 0;
		int all_frames = (int) playable.getProperty("mp3.length.frames");
		long frames_played = (long) audioStream.properties().get("mp3.frame");
		long ret = frames_played*100/all_frames;
		return (int) ret;
	}

	/**
	 * Add a new {@list PlayerListener}.
	 * 
	 * @param l the PlayerListener to add
	 */
	public void addPlayerListener(PlayerListener l) {
		listeners.add(l);
	}

	/**
	 * Get the current state of our player (e.g. PLAY, STOP etc).
	 * 
	 * @return player's state
	 */
	protected State getState() {
		return state;
	}
	
	/**
	 * Return true if the player is in STOP state.
	 * 
	 * @return player's state
	 */
	public boolean isStopped() {
		return (state == State.STOP);
	}
	
	/**
	 * Return true if the player is in PAUSED state.
	 * 
	 * @return player's state
	 */
	public boolean isPaused() {
		return (state == State.PAUSE);
	}
	
	/**
	 * Move forward a predefined amount of time.
	 */
	public void moveForward() {
		if(isPlay())
			move_fwd = true;
	}
	
	/**
	 * Return true if the player is in PLAY state.
	 * 
	 * @return player's state
	 */
	public boolean isPlay() {
		return (state == State.PLAY);
	}
}