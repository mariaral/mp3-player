package player;

import java.net.URL;

import javazoom.spi.mpeg.sampled.convert.DecodedMpegAudioInputStream;

/**
 * The {@link Playable} interface. Each object that can be playbe
 * by our {@link Player} has to implement this interface.
 * 
 * @author Maria
 *
 */
public interface Playable {
	
	public DecodedMpegAudioInputStream getInputStream();
	public Playable getNext();    // Next playable object
	public String getTitle();	  // Title of the stream
	public String getAuthor();	  // Name of the artist of the stream
	public String getAlbum();	  // Name of the album of the stream
	public String getArtistBio(); // Artist biography
	public URL getAlbumURL();     // Album image url
	    
    public Object getProperty(String key);
    	
}