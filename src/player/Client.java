package player;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * A Simple HTML client that constructs the URL, sends the request
 * and receives a response.
 * 
 * @author Maria
 *
 */
public class Client {

	private String host;
	private String query;
	private Document doc;

	/**
	 * Class constructor.
	 * 
	 * @param host the host to connect to
	 * @param query the POST query
	 */
	public Client(String host, String query) {
		doc = null;
		this.host = host;
		this.query = query;
	}

	/**
	 * Class constructor.
	 * 
	 * @param host the host to connect to
	 */
	public Client(String host) {
		this(host, "");
	}

	/**
	 * Add a new argument to the POST query.
	 * 
	 * @param name the name of the POST argument
	 * @param value the value of the POST argument
	 */
	public void addArg(String name, String value) {

		String q;
		try {
			q = String.format("%s=%s&", URLEncoder.encode(name, "UTF-8"),
					URLEncoder.encode(value, "UTF-8"));
			query += q;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Send the request.
	 */
	public void sendRequest() {
		try {
			URL url = new URL(host);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setDoOutput(true);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			con.setRequestProperty("charset", "utf-8");
			con.setRequestProperty("Content-Length",
					"" + Integer.toString(query.getBytes().length));

			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(query);
			wr.flush();
			wr.close();
						
			DataInputStream rd = new DataInputStream(con.getInputStream());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(rd);
			con.disconnect();
		} catch (IOException e) {
			// continue
		} catch (ParserConfigurationException | SAXException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get server's response. If there was not a valid response,
	 * return null.
	 * 
	 * @return server's response
	 */
	public Document getResponse() {
		return doc;
	}
}
