package player;

/**
 * A List listener that raise events every time a list
 * is updated.
 * 
 * @author Maria
 *
 */
public interface ListListener {
	void listupdated();
}
