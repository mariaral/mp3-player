package player;

import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringEscapeUtils;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * This class implements a client to the LastFM's
 * API and retrieves the artist biography and the
 * album cover for a given artist and track.
 * 
 * @author Maria
 *
 */
public class LastFM {
	private String artist_bio;
	private String album_name;
	private URL album_url;
	private String api_key = "e621f05d3c2181933f00be534d58a625";
	
	/**
	 * Class constructor.
	 * 
	 * @param artist artist's name
	 * @param track track's name
	 */
	public LastFM(String artist, String track) {
		artist_bio = "";
		album_name = "<Unknown album>";
		album_url = unknownURL();
		initArtistBio(artist);
		initAlbumInfo(artist, track);
	}
	
	/**
	 * Get the artist biography in "text/html" format.
	 * 
	 * @return artist's biography
	 */
	public String getArtistBio() {
		return artist_bio;
	}
	
	/**
	 * Get the album name.
	 * 
	 * @return the album name
	 */
	public String getAlbumName() {
		return album_name;
	}
	
	/**
	 * Get the URL with the album cover.
	 * 
	 * @return album cover url
	 */
	public URL getAlbumURL() {
		return album_url;
	}
	
	/**
	 * Get the URL with a predefined album cover that can be used
	 * when the original one was not found.
	 * 
	 * @return unknown album cover url
	 */
	static public URL unknownURL() {
		URL unknown_url = null;
		try {
			unknown_url = new URL("http://dougscripts.com/itunes/pix/jt_icon_128x128.png");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return unknown_url;
	}
	
	private void initArtistBio(String artist) {
		Client client = new Client("http://ws.audioscrobbler.com/2.0/");
		client.addArg("api_key", api_key);
		client.addArg("method", "artist.getinfo");
		client.addArg("artist", artist);
		client.sendRequest();
		
		Document fdoc = client.getResponse();
		if(fdoc == null)
			return;
		fdoc.setXmlStandalone(true);
	    DOMSource domSource = new DOMSource(fdoc);
	    StringWriter writer = new StringWriter();
	    StreamResult result = new StreamResult(writer);
	    TransformerFactory tf = TransformerFactory.newInstance();
	    String xsl = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
	        "<html xsl:version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">" +
	    	"<font size=\"5\">" +
	    	"<xsl:value-of select=\"lfm/artist/bio/content\"/>" +
	        "</font></html>";
	    Source src = new StreamSource(new StringReader(xsl));
	    try {
	    	Templates template = tf.newTemplates(src);
	    	Transformer transformer = template.newTransformer();
	    	transformer.setOutputProperty(OutputKeys.ENCODING,  "UTF-8");
			transformer.transform(domSource, result);
		} catch (TransformerException e1) {
			e1.printStackTrace();
		}
	    
	    artist_bio = StringEscapeUtils.unescapeHtml4(writer.toString());
	}
	
	private void initAlbumInfo(String artist, String track) {
		int i;
		
		Client client = new Client("http://ws.audioscrobbler.com/2.0/");
		client.addArg("api_key", api_key);
		client.addArg("method", "track.getinfo");
		client.addArg("artist", artist);
		client.addArg("track", track);
		client.sendRequest();
		
		Document fdoc = client.getResponse();
		if(fdoc == null)
			return;
		
		try {
			Element album_elem = (Element) fdoc.getElementsByTagName("album").item(0);
			NodeList image_list = album_elem.getElementsByTagName("image");
			for(i=0; i<image_list.getLength(); i++) {
				Element elem = (Element) image_list.item(i);
				if(elem.getAttribute("size").equals("large")) {
					try {
						album_url = new URL(elem.getTextContent());
						break;
					} catch (MalformedURLException e1) {
						e1.printStackTrace();
					}
				}
			}
			Element title_elem = (Element) album_elem.getElementsByTagName("title").item(0);
			album_name = title_elem.getTextContent();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
