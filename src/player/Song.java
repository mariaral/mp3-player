package player;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Map;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import javazoom.spi.mpeg.sampled.convert.DecodedMpegAudioInputStream;

/**
 * This class represents a Song (in mp3 format).
 * It reads the song file from the filesystem and
 * retrieves its Author, Album and Title. Moreover it
 * retrieves informations about this song from LastFM.
 * <p>
 * Each {@link Song} is a {@link Playable} object.
 * 
 * @author Maria
 *
 */
public class Song implements Playable {

	private File song_file;

	private String title;
	private String author;
	private Map<String, Object> properties;
	
	LastFM lastfm;

	/**
	 * Class constructor.
	 * 
	 * @param file The song's File object
	 * @throws UnsupportedAudioFileException
	 */
	public Song(File file) throws UnsupportedAudioFileException {
		song_file = file;
		try {
			AudioFileFormat baseFileFormat = AudioSystem.getAudioFileFormat(file);
			properties = baseFileFormat.properties();
			checkMP3(baseFileFormat);
			getBasicProperties();
			lastfm = new LastFM(author, title);
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Class constructor.
	 * 
	 * @param filename The song's filename
	 * @throws UnsupportedAudioFileException
	 */
	public Song(String filename) throws UnsupportedAudioFileException {
		this(new File(filename));
	}

	private void checkMP3(AudioFileFormat fileFormat) throws UnsupportedAudioFileException {
		if(!fileFormat.getType().toString().equals("MP3")) {
			throw new UnsupportedAudioFileException("Only Mp3 supporting");
		}
	}

	private void getBasicProperties() {
		title = (String) properties.get("title");
		author = (String) properties.get("author");
	}

	/**
	 * Get the {@link DecodedMpegAudioInputStream} of this song.
	 * If the input stream cannot be retrieved return null.
	 * 
	 * @return input stream for the song
	 */
	public DecodedMpegAudioInputStream getInputStream() {
		try {
		AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(song_file);
		AudioFormat baseAudioFormat = audioInputStream.getFormat();
		AudioFormat decodedAudioFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, baseAudioFormat.getSampleRate(),
				16, baseAudioFormat.getChannels(),
				baseAudioFormat.getChannels() * 2, baseAudioFormat.getSampleRate(), false);
		DecodedMpegAudioInputStream decodedInputStream =
				new DecodedMpegAudioInputStream(decodedAudioFormat, audioInputStream);
		return decodedInputStream;
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Get the next song to be played. Since a
	 * song doesn't know what should be played next,
	 * always return null.
	 * 
	 * @return the next Playable object
	 */
	public Playable getNext() {
		return null;
	}

	/**
	 * Get the title of the song. If no title can
	 * be retrieved return "<Unknown title>".
	 * 
	 * @return the title of the song
	 */
	public String getTitle() {
		if(title == null)
			return("<Unknown title>");
		else if(title.isEmpty())
			return("<Unknown title>");
		else
			return title;
	}

	/**
	 * Get the artist of the song. If no artist can
	 * be retrieved return "<Unknown artist>".
	 * 
	 * @retrn the artist of the song
	 */
	public String getAuthor() {
		if(author == null)
			return("<Unknown artist>");
		else if(author.isEmpty())
			return("<Unknown artist>");
		else
			return author;
	}

	/**
	 * Get the album of the song. If no album can
	 * be retrieved return "<unknown album>".
	 * 
	 * @return the album of the song
	 */
	public String getAlbum() {
		return lastfm.getAlbumName();
	}
	
	/**
	 * Get the artist's biography for this song.
	 * 
	 * @return artist's biography
	 */
	public String getArtistBio() {
		return lastfm.getArtistBio();
	}
	
	/**
	 * Get the album's url for this song.
	 * 
	 * @return album's url
	 */
	public URL getAlbumURL() {
		return lastfm.getAlbumURL();
	}

	/**
	 * Get a property for this song.
	 * 
	 * @param key the name of the property
	 * @return the value of the property
	 */
	public Object getProperty(String key) {
		return properties.get(key);
	}
}
