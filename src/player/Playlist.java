package player;

import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javazoom.spi.mpeg.sampled.convert.DecodedMpegAudioInputStream;

/**
 * This class implements a linked list of {@link Playable}
 * objects, hence it is a playlist. Moreover, each
 * {@link Playlist} is a {@link Playable} object itself.
 * 
 * @author Maria
 *
 */
public class Playlist implements Playable {

	private LinkedList<Playable> playlist;
	private Playable current;
	private List<ListListener> listeners;

	/**
	 * Class constructor.
	 */
	public Playlist() {
		playlist = new LinkedList<Playable>();
		current = null;
		listeners = new ArrayList<ListListener>();
	}

	/**
	 * Add a new {@link Playable} object to the playlist.
	 * 
	 * @param p the playable object to add
	 */
	public void add(Playable p) {
		if(playlist.isEmpty())
			current = p;
		playlist.addLast(p);
		for(ListListener l : listeners)
			l.listupdated();
	}

	/**
	 * Returns the current {@link Playable} object.
	 * 
	 * @return current playable object
	 */
	public Playable getCurrent() {
		return current;
	}

	/**
	 * Set the object at the given position as the
	 * current one.
	 * 
	 * @param index set current position to index
	 */	
	public void setCurrent(int index) {
		try {
			current = playlist.get(index);
		} catch (IndexOutOfBoundsException e) {
			return;
		}
	}

	/**
	 * Get the next {@link Playable} object.
	 * 
	 * @return the next Playable object
	 */
	public Playable getNext() {
		Playable next;
		next = current.getNext();
		if(next == null) {
			int current_index = playlist.indexOf(current);
			try {
				current = playlist.get(current_index+1);
				next = this;
			} catch (IndexOutOfBoundsException e) {
				current = playlist.getFirst();
				next = null;
			}
		}
		return next;
	}
	
	/**
	 * Reset the playlist (remove all its elements).
	 */
	public void reset() {
		playlist.removeAll(getAll());
		for(ListListener l : listeners)
			l.listupdated();
	}
	
	/**
	 * Get all the {@link Playable} objects inside
	 * this playlist as a {@link LinkedList}.
	 * 
	 * @return the objects of the playlist
	 */
	public LinkedList<Playable> getAll() {
		return playlist;
	}

	/**
	 * Get the {@link DecodedMpegAudioInputStream} of the
	 * current {@link Playable} object. If there are no
	 * elements in the playlist, return null.
	 * 
	 * @return input stream for the current object
	 */
	public DecodedMpegAudioInputStream getInputStream() {
		if(current != null)
			return current.getInputStream();
		else
			return null;
	}

	/**
	 * Get the title of the current {@link Playable} object.
	 * If there are no elements in the playlist, return the
	 * empty String.
	 * 
	 * @return the title of the current object
	 */
	public String getTitle() {
		if(current != null)
			return current.getTitle();
		else
			return "";
	}

	/**
	 * Get the artist of the current {@link Playable} object.
	 * If there are no elements in the playlist, return the
	 * empty String.
	 * 
	 * @return the artist of the current object
	 */
	public String getAuthor() {
		if(current != null)
			return current.getAuthor();
		else
			return "";
	}

	/**
	 * Get the album name of the current {@link Playable} object.
	 * If there are no elements in the playlist, return the
	 * empty String.
	 * 
	 * @return the album name of the current object
	 */
	public String getAlbum() {
		if(current != null)
			return current.getAlbum();
		else
			return "";
	}
	
	/**
	 * Get the artist's biography for the current {@link Playable}
	 * object. If there are no elements in the playlist, return
	 * the empty String.
	 * 
	 * @return artist's biography for the current object
	 */
	public String getArtistBio() {
		if(current != null)
			return current.getArtistBio();
		else
			return "";
	}
	
	/**
	 * Get the album's url for the current {@link Playable}
	 * object. If there are no elements in the playlist,
	 * return the empty String.
	 * 
	 * @return album's url for the current object
	 */
	public URL getAlbumURL() {
		if(current != null)
			return current.getAlbumURL();
		else
			return LastFM.unknownURL();
	}

	/**
	 * Get a property from the current {@link Playable}
	 * object. If there are no elements in the playlist,
	 * return null.
	 * 
	 * @param key the name of the property
	 * @return the value of the property
	 */
	public Object getProperty(String key) {
		if(current != null)
			return current.getProperty(key);
		else
			return null;
	}

	/**
	 * Add a new {@link ListListener}.
	 * 
	 * @param l the ListListener to add
	 */
	public void addListListener(ListListener l) {
		listeners.add(l);
	}
}
