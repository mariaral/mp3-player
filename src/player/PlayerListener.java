package player;

/**
 * A Player Listener that raises events every time
 * the state of the player changes.
 * 
 * @author Maria
 *
 */
public interface PlayerListener {
	void playerStatusChanged(Player.Event e);
}
