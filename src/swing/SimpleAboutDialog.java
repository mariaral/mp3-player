package swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This is a simple about dialog,
 * implemented using Java's swing libraries.
 * 
 * @author Maria
 *
 */
public class SimpleAboutDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	/**
	 * Class constructor.
	 * 
	 * @param parent the parent frame
	 */
	public SimpleAboutDialog(JFrame parent) {
		super(parent, "About Dialog", true);
		
		Box b = Box.createVerticalBox();
		b.add(Box.createGlue());
		b.add(new JLabel("A Java Media Player"));
		b.add(new JLabel("By Maria Ralli"));
		b.add(new JLabel("At medialab.ntua.gr"));
		b.add(Box.createGlue());
		getContentPane().add(b, "Center");
		
		JPanel p2 = new JPanel();
	    JButton ok = new JButton("Ok");
	    p2.add(ok);
	    getContentPane().add(p2, "South");

	    ok.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent evt) {
	        setVisible(false);
	      }
	    });

	    setSize(250, 150);
	}
}
