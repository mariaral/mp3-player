package swing;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import player.LastFM;
import player.ListListener;
import player.Playable;
import player.Player;
import player.PlayerListener;
import player.Playlist;
import player.Song;


/**
 * This is the class the implements the
 * Player's GUI components. It uses
 * Java's swing libraries.
 * 
 * @author Maria
 *
 */
public class PlayerGui {

	private Player player;
	private Playlist playlist;
	private JProgressBar pbar;
	private JLabel time;
	private JTextPane info;
	private JLabel album_cover;
	private JLabel song;
	private JLabel artist;
	private JLabel album;

	/* Configure GridBagConstraints for each component */	
	private void configConstraints(int x, int y, int w, int h, double wx, double wy,
			GridBagConstraints c, JComponent comp) {

		c.gridx = x; c.gridy = y;
		c.gridheight = h;
		c.gridwidth = w;
		c.weightx = wx; c.weighty = wy;
		comp.setPreferredSize(new Dimension(10,10));
		comp.setSize(new Dimension(10, 10));
	}

	private void makeTranparent(JButton b) {
		b.setOpaque(false);
		b.setContentAreaFilled(false);
		b.setBorderPainted(false);
		b.setFocusPainted(false); 
	}
	
	private ImageIcon scaledMenuImage(String img) {
		ImageIcon icon = new ImageIcon(img);
		Image image = icon.getImage().getScaledInstance(32, 32, Image.SCALE_SMOOTH);
		return (new ImageIcon(image));
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void createAndShowPlayer() {

		/* Image for frame */
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Image icon = toolkit.getImage("icons/music.png");

		/* Create the frame */
		final JFrame frame = new JFrame("Mp3 Player");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);
		frame.setIconImage(icon);
		/* Locate at the center of the screen */
		frame.setLocationRelativeTo(null);

		/* Create menubar with file and help */		
		JMenuBar menubar = new JMenuBar();

		/* File menu */
		JMenu file = new JMenu("File");
		JMenuItem open = new JMenuItem("Open", scaledMenuImage("icons/open.png"));
		open.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fc = new JFileChooser();
				FileNameExtensionFilter mp3filter = new FileNameExtensionFilter("MP3 file", "mp3");
				fc.setFileFilter(mp3filter);
				fc.setAcceptAllFileFilterUsed(false);
				fc.setMultiSelectionEnabled(true);
				int retval = fc.showOpenDialog(frame);
				if (retval == JFileChooser.APPROVE_OPTION) {
					try {
						File[] files = fc.getSelectedFiles();
						for (File f : files) {						
							Song song = new Song(f);
							playlist.add(song);
						}
					} catch (UnsupportedAudioFileException e) {
						System.out.println(e.getMessage());
					}
				}
			}
		});
		file.add(open);
		JMenuItem close = new JMenuItem("Close", scaledMenuImage("icons/close.png"));
		close.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				player.stop();
				playlist.reset();
			}

		});
		file.add(close);
		JMenuItem exit = new JMenuItem("Exit", scaledMenuImage("icons/exit.png"));
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		file.add(exit);
		menubar.add(file);

		/* Help menu */
		JMenu help = new JMenu("Help");
		menubar.add(help);
		JMenuItem about = new JMenuItem("About", scaledMenuImage("icons/about.png"));
		help.add(about);
		frame.setJMenuBar(menubar);
		final JDialog about_dialog = new SimpleAboutDialog(frame);
		about.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				about_dialog.setVisible(true);
			}
		});

		/* Set default panel to frame */
		JPanel pane = new JPanel();
		frame.setContentPane(pane);
		pane.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;

		/* Add album cover */
		album_cover = new JLabel();
		configConstraints(0,0,1,4,.2,0.2,c,album_cover);
		pane.add(album_cover, c);
		updateAlbumImg();
		
		/* Label with song name */
		song = new JLabel();
		song.setFont(new Font("SansSerif", Font.BOLD, 20));
		configConstraints(1,0,5,1,.8,0.05,c,song);
		pane.add(song, c);
		/* Label with artist name */
		artist = new JLabel();
		artist.setFont(new Font("SansSerif", Font.ITALIC, 15));
		configConstraints(1,1,4,1,0.7,0.05,c,artist);
		pane.add(artist, c);
		/* Label with album name */
		album = new JLabel();
		album.setFont(new Font("SansSerif", Font.ITALIC, 12));
		configConstraints(1,2,4,1,0.7,0.05,c,album);
		pane.add(album, c);
		updateSongInfo();
		
		/* Label with time elapsed */
		time = new JLabel();
		time.setFont(new Font("SansSerif", Font.PLAIN, 20));
		configConstraints(5,1,1,2,0.1,0.1,c,time);
		pane.add(time, c);
		/* Progress bar */
		pbar  = new JProgressBar();
		pbar.setBorderPainted(true);
		pbar.setMinimum(0);
		pbar.setMaximum(100);
		configConstraints(1,3,5,1,0.8,0.05,c,pbar);
		pane.add(pbar, c);
		/* Create buttons */
		final JButton play = new JButton(new ImageIcon("icons/play.png"));
		makeTranparent(play);
		configConstraints(1,4,1,1,0.1,0.1,c,play);
		play.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(player.isPlay())
					player.pause();
				else
					player.play(playlist);
			}
		});
		pane.add(play,c);
		JButton stop = new JButton(new ImageIcon("icons/stop.png"));
		configConstraints(2,4,1,1,0.1,0.1,c,stop);
		makeTranparent(stop);
		stop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				player.stop();
			}
		});
		pane.add(stop,c);
		JButton fwd = new JButton(new ImageIcon("icons/fwd.png"));
		makeTranparent(fwd);
		configConstraints(3,4,1,1,0.1,0.1,c,fwd);
		fwd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				player.moveForward();
			}
		});
		pane.add(fwd, c);
		// Empty space
		JLabel empty_space = new JLabel();
		configConstraints(4, 4, 1, 1, 0.5, 0.1, c, empty_space);
		pane.add(empty_space, c);
		
		/* List with songs */
		final DefaultListModel listModel = new DefaultListModel();
		final JList list = new JList(listModel);
		list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		list.setVisibleRowCount(-1);
		JScrollPane listScroller = new JScrollPane(list);
		listScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		listScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		listScroller.setPreferredSize(new Dimension(10,10));
		configConstraints(0,4,1,2,0.2,0.8,c,list);
		pane.add(listScroller,c);
		playlist.addListListener(new ListListener() {
			public void listupdated() {
				listModel.removeAllElements();
				for(Playable song : playlist.getAll()) {
					listModel.addElement(song.getTitle());
				}
				list.revalidate();
				list.repaint();
			}
		});
		list.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
		        JList list = (JList)evt.getSource();
		        if (evt.getClickCount() == 2) {
		            int index = list.locationToIndex(evt.getPoint());
		            playlist.setCurrent(index);
		            player.stop();
		            player.play(playlist);
		        }
		    }
		});
		
		/* Artist biography */
		info = new JTextPane();
		info.setContentType("text/html");
		info.setEditable(false);
		info.setOpaque(false);
		ToolTipManager.sharedInstance().registerComponent(info);
		JScrollPane infoScroller = new JScrollPane(info);
		infoScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		infoScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		infoScroller.setPreferredSize(new Dimension(10, 10));
	    info.addHyperlinkListener(new HyperlinkListener() {
	    	public void hyperlinkUpdate(HyperlinkEvent e) {
	    		if(e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
	    			if(Desktop.isDesktopSupported()) {
	    				try {
	    					Desktop.getDesktop().browse(e.getURL().toURI());
	    				}
	    				catch (IOException | URISyntaxException e1) {
	    					e1.printStackTrace();
	    				}
	    			}
	    		}
	    	}
	    });
		configConstraints(1,5,5,1,0.8,0.7,c,info);
		pane.add(infoScroller,c);
			
		player.addPlayerListener(new PlayerListener() {
			public void playerStatusChanged(Player.Event e) {
				switch(e) {
				case INIT_PLAY: 
					song.setText(playlist.getTitle());
					song.repaint();
					artist.setText(playlist.getAuthor());
					artist.repaint();
					album.setText(playlist.getAlbum());
					album.repaint();
					play.setIcon(new ImageIcon("icons/pause.png"));
					break;
				case RESUMED:
					play.setIcon(new ImageIcon("icons/pause.png"));
					break;
				case STOPPED:
					play.setIcon(new ImageIcon("icons/play.png"));
					break;
				case PAUSED:
					play.setIcon(new ImageIcon("icons/play.png"));
					break;
				}
				updateArtistBio();
				updateAlbumImg();
				updateSongInfo();
			}
		});

		frame.setVisible(true);
	}
	
	private void updateProgBar() {
		pbar.setValue(player.getPercentage());
		time.setText(player.getTimeElapsed());
	}
	
	private void runProgBar() {
		new Thread(new Runnable() {
			public void run() {
				while(true) {
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							updateProgBar();
						}
					});
					try { Thread.sleep(500); } catch (InterruptedException e) {}
				}
			}
		}).start();
	}
	
	private void updateArtistBio() {
		String artist_bio;
		if(player.isStopped())
			artist_bio = "";
		else
			artist_bio = playlist.getArtistBio();
		info.setText(artist_bio);
	}
	
	private void updateAlbumImg() {
		URL image_url;
		if(player.isStopped())
			image_url = LastFM.unknownURL();
		else
			image_url = playlist.getAlbumURL();
		album_cover.setIcon(new StretchIcon(image_url));
	}
	
	private void updateSongInfo() {
		String song_text = "";
		String artist_text = "";
		String album_text = "";
		if(!player.isStopped()) {
			song_text = playlist.getTitle();
			artist_text = playlist.getAuthor();
			album_text = playlist.getAlbum();
		}
		song.setText(song_text);
		artist.setText(artist_text);
		album.setText(album_text);
	}

	public PlayerGui() throws UnsupportedAudioFileException {
		player = new Player();
		playlist = new Playlist();
	}

	/**
	 * Main method.
	 * 
	 * @param args command line arguments
	 * @throws UnsupportedAudioFileException
	 */
	public static void main(String[] args) throws UnsupportedAudioFileException {
		final PlayerGui playerGui = new PlayerGui();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				playerGui.createAndShowPlayer();
				playerGui.runProgBar();
			}
		});
	}
}